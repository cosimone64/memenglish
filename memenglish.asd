(defpackage memenglish-asd
  (:use :cl :asdf))

(in-package :memenglish-asd)

(defsystem :memenglish
  :description "A translator to various funny dialects"
  :author "cosimone64"
  :license "AGPL3+"
  :components ((:file "memenglish")
               (:file "tests"))
  :depends-on (:alexandria :cl-ppcre :ltk))
