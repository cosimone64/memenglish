;;;; Copyright (C) 2020-2021 cosimone64

;;;; This program is free software: you can redistribute it and/or
;;;; modify it under the terms of the GNU Affero General Public License
;;;; as published by the Free Software Foundation, either version 3 of
;;;; the License, or (at your option) any later version.

;;;; You should have received a copy of the GNU AGPLv3 with this software,
;;;; if not, please visit <https://www.gnu.org/licenses/>

(defpackage memenglish-tests
  (:use :common-lisp :memenglish)
  (:export :run-tuscan-tests))

(in-package :memenglish-tests)

(defmacro run-test (expression expected &key (test #'eql))
  "Returns T if the value of EXPRESSION equals EXPECTED.
TEST can name the function used to compare the results.  Defaults to EQL"
  `(let ((actual ,expression))
     (if (funcall ,test actual ,expected)
         (format t "Testing expression ~a succeeded.~%" (quote ,expression))
         (error
          (format nil
                  "Testing expression ~a failed.~%Expected: ~a~%Actual: ~a"
                  (quote ,expression)
                  ,expected
                  actual)))))

(defun tuscanize-text-test (arg expected)
  "Test TUSCANIZE-TEXT."
  (run-test (translate arg :tuscan) expected :test #'string=))

(defun run-tuscan-tests ()
  (tuscanize-text-test "un caffe" "un caffe")
  (tuscanize-text-test "due caffe" "du'haffe")
  (tuscanize-text-test "tre caffe" "tre caffe")
  (tuscanize-text-test "quattro caffe" "quattro haffe")
  (tuscanize-text-test "la coca cola con la cannuccia corta"
                       "la hoha hola hon la hannuccia horta"))
