(defconstant +executable-name+ "memenglish.exe")

(load "package.lisp")
(load "memenglish.lisp")

#+sbcl
(sb-ext:save-lisp-and-die +executable-name+
                          :executable t
                          :toplevel 'com.cc.memenglish:memeify-repl)
