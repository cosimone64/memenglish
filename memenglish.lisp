;;;; Copyright (C) 2020-2021 cosimone64

;;;; This program is free software: you can redistribute it and/or
;;;; modify it under the terms of the GNU Affero General Public License
;;;; as published by the Free Software Foundation, either version 3 of
;;;; the License, or (at your option) any later version.

;;;; You should have received a copy of the GNU AGPLv3 with this software,
;;;; if not, please visit <https://www.gnu.org/licenses/>

(declaim (optimize (speed 0) (debug 3) (safety 3)))

(defpackage :memenglish
  (:use :common-lisp)
  (:export :term-table :current-language :set-language :suffix-table
           :translate :memeify-image :translate-file :memeify-repl
           :memeify-gui :load-database))

(in-package :memenglish)

(defparameter *language-databases* (list :memenglish "memenglish-database.lisp"
                                         :tuscan "tuscan-database.lisp"))

(defvar *current-language* :memenglish)

(deftype token-type () '(member :consonant :vowel :diphtong :whitespace))

(defclass token ()
  ((type :type token-type :reader toktype :initarg :toktype
         :initform (error "Must provide token type"))
   (contents :type string :reader contents :initarg :contents
             :initform (error "Must provide contents"))
   (index :type fixnum :reader index :initarg :index
          :initform (error "Must provide index")))
  (:documentation "Representation of an individual tuscan text token"))

(defgeneric translate-dispatch (text language)
  (:documentation "Translate TEXT into LANGUAGE"))

(defun drop-first-char (word)
  "Return WORD without its first character."
  (subseq word 1 (length word)))

(defun drop-last-char (word)
  "Return WORD without its last character."
  (subseq word 0 (1- (length word))))

(defun concat (&rest strings)
  "Concatenate STRINGS into a single string.
Essentially just a shorthand for calling concatenate 'string."
  (apply #'concatenate 'string strings))

(defun ends-with (word suffix)
  "Return non-NIL if WORD ends with SUFFIX, NIL otherwise."
  (alexandria:ends-with-subseq suffix word :test #'string=))

(defun drop-suffix (word suffix-len)
  "Return WORD, but without the last SUFFIX-LEN characters."
  (subseq word 0 (- (length word) suffix-len)))

(defun substitute-suffix (word old-len new)
  "Substitute the last OLD-LEN elements in WORD with the NEW suffix."
  (concat (drop-suffix word old-len) new))

(defun table-from-pairs (pairs)
  "Return a hash table from the PAIRS list.
PAIRS is a list where each element must consist of a list of exactly two
elements."
  (when (some (lambda (p) (/= 2 (length p))) pairs)
    (error "PAIRS list is not well formed"))
  (loop with ht = (make-hash-table :test #'equal)
        for x in pairs
        do (setf (gethash (first x) ht) (second x))
        finally (return ht)))

(defun current-language ()
  "Return the currently enabled language for translation."
  *current-language*)

(defun load-parameters (filename)
  "Load translator parameters from disk."
  (with-open-file (file filename :direction :input)
    (read file)))

(let ((term-tables (make-hash-table))
      (suffix-tables (make-hash-table)))
  (defun get-word-from-table (word)
    "Return a possible meme substitution for WORD."
    (let ((table (gethash *current-language* term-tables)))
      (multiple-value-bind (meme-candidate presentp) (gethash word table)
        (when presentp
          (if (not (listp meme-candidate))
              meme-candidate
              (nth (random (length meme-candidate)) meme-candidate))))))

  (defun get-meme-ending (word)
    "Return WORD with the appropriate suffix substitution, if present.
If no such substitution is present in the ending table, WORD itself is returned
with no modifications."
    (loop with table = (gethash *current-language* suffix-tables)
          for old-suff of-type string being the hash-keys in table
          if (ends-with word old-suff)
            return (let* ((entry (gethash old-suff table))
                          (new-suff (if (typep entry 'list)
                                        (nth (random (length entry)) entry)
                                        entry)))
                     (substitute-suffix word (length old-suff) new-suff))
          finally (return word)))

  (defun load-database (&rest langs)
    "Load the term/suffix databases for langs."
    (loop for lang in langs
          for db = (getf *language-databases* lang)
          unless db
            do (error (format nil "Language ~a not available" lang))
          do (let* ((params (load-parameters db))
                    (term-tbl (table-from-pairs (getf params :term-table)))
                    (suffix-tbl (table-from-pairs (getf params :suffix-table))))
               (psetf (gethash lang term-tables) term-tbl
                      (gethash lang suffix-tables) suffix-tbl))
             (setf *current-language* lang)))

  (defun set-language (lang)
    "Set the current active language for translation to LANG."
    (when (not (and (gethash lang term-tables)
                    (gethash lang suffix-tables)))
      (load-database lang))
    (setf *current-language* lang)))

(defun remove-whitespace-after-apostrophes (text)
  "Return a version of TEXT without whitespace after apostrophes."
  ;;TODO: Could detect other whitespace characters besides spaces...
  (nth-value 0 (cl-ppcre:regex-replace-all "' " text "'")))

(defun split-process-recombine (text separator format-separator process)
  "Splits TEXT according to SEPARATOR, PROCESSes each part and joins them
back together via FORMAT-SEPARATOR."
  (setf text (cl-ppcre:split separator text)
        text (mapcar process text))
  (format nil (concat "~{~a~^" format-separator "~}") text))

(defun memenglish-pluralize (word)
  "If WORD is an English plural word, return the corresponding meme plural.
Otherwise, just return WORD unmodified."
  (if (and (not (ends-with word "ss"))
           (ends-with word "s"))
      (concat (drop-last-char word) "z")
      word))

(defun correct-word (word capitalized-p
                     &optional punctuation-at-start punctuation-at-end)
  "Return a possibly capitalized WORD with PUNCTUATION-AT-END.
This is used to reconstruct how the word originally looked like in the text."
  (when capitalized-p
    (setf word (string-capitalize word)))
  (setf word (memenglish-pluralize word))
  (when punctuation-at-start
    (setf word (concat (string punctuation-at-start) word)))
  (when punctuation-at-end
    (setf word (concat (string punctuation-at-end) word)))
  word)

(defun meme-word-substitute (word)
  "Return the appropriate string substitution for WORD.
If not present in the table, the ending is processed."
  (or (get-word-from-table word) (get-meme-ending word)))

(defun memeify-word (word)
  "From WORD, return its memeified version. WORD must be nonempty."
  (let ((punctuation-list '(""  "\'" "." "\\" "?" ",")))
    (when (member word punctuation-list :test #'string=)
      (return-from memeify-word word))
    (let ((punctuation-at-start (find (elt word 0) punctuation-list))
          (punctuation-at-end (find (elt word (1- (length word)))
                                    punctuation-list))
          (capitalized-p (upper-case-p (elt word 0))))
      (when punctuation-at-start
        (setf word (drop-first-char word)))
      (when punctuation-at-end
        (setf word (drop-last-char word)))
      (setf word (string-downcase word)
            word (meme-word-substitute word))
      (correct-word word capitalized-p punctuation-at-start
                    punctuation-at-end))))

(defun memeify-line (line)
  "Return a string containing the translation for LINE.
LINE is assumed not to contain newlines.  Because of this, MEMEIFY-LINE should
be used only as an internal helper function."
  (split-process-recombine line " " " " #'memeify-word))

(defun memeify-text (text)
  "Return a string containing the translation for TEXT.
TEXT may contain newlines."
  (remove-whitespace-after-apostrophes
   (split-process-recombine text "\\n" "~%" #'memeify-line)))

(defun denoise-line (line)
  "Remove some useless characters from LINE.
LINE is assumed not to contain newlines.  This function should be used only as
an internal helper function."
  (split-process-recombine line " " ""
                           (lambda (word)
                             (if (= (length word) 1) "" word))))

(defun denoise-text (text)
  "Remove some useless characters from TEXT.
Useful to clean up some leftover characters from the output of an OCR engine.
TEXT may contain newlines."
  (split-process-recombine text "\\n" "~%" #'denoise-line))

(defun translate-file (filename)
  "Return a string representing the translation for the text in file FILENAME."
  (let ((result (make-array 0 :element-type 'character :fill-pointer t)))
    (flet ((push-line (new)
             (loop for c across new do (vector-push-extend c result))
             (vector-push-extend #\Newline result)))
      (with-open-file (file filename :direction :input)
        (loop for line = (read-line file nil)
              while line
              do (push-line (translate line)))))
    result))

(defun memeify-repl ()
  "Basic REPL to be used standalone."
  (loop
   (format t "> ")
   (let ((line (read-line)))
     (if (string= (string-downcase line) "exit")
         (return)
         (format t "~a~%" (translate line))))))

(defun memeify-gui ()
  "Spawn a basic GUI to interact with the engine."
  (let ((ltk:*wish-args* (list "-name" "Memenglish")))
    (ltk:with-ltk ()
      (macrolet ((make-button (language)
                   `(make-instance 'ltk:button
                                   :text (concat "Translate into "
                                                 (string-downcase
                                                  (string ,language)))
                                   :command (lambda ()
                                              (setf *current-language* ,language)
                                              (setf (ltk:text output-text)
                                                    (translate
                                                     (ltk:text input-text)))))))
        (let* ((input-text (make-instance 'ltk:text))
               (output-text (make-instance 'ltk:text))
               (memenglish-button (make-button :memenglish))
               (tuscanize-button (make-button :tuscan)))
          (ltk:pack memenglish-button :side :bottom)
          (ltk:pack tuscanize-button :side :bottom)
          (ltk:pack input-text :side :left)
          (ltk:pack output-text :side :right))))))

(defun consonantp (char)
  "Return non-NIL if CHAR is a consonant, NIL otherwise."
  (let ((consonants '(#\b #\c #\d #\f #\g #\h #\j #\k #\l #\m #\n #\p
                      #\q #\r #\s #\t #\v #\x #\y #\z)))
    (member (char-downcase char) consonants)))

(defun diphtongp (str)
  "Return non-NIL if STR represents a diphtong, NIL otherwise."
  (let ((diphtongs '("ae" "ai" "ie" "ue"))
        (spaceless-str (remove-if (lambda (c) (char= c #\Space)) str)))
    (member spaceless-str diphtongs :test #'string=)))

(defun next-non-whitespace (str i)
  "Return index of the first non-whitespace character in STR from I.
If none is found, NIL is returned."
  (cond ((>= i (length str)) nil)
        ((alphanumericp (char str i)) i)
        (t (next-non-whitespace str (1+ i)))))

(defun make-token (symbol contents index)
  (make-instance 'token :toktype symbol :contents contents :index index))

;; Methods to use if tokens are represented as lists
(defmethod toktype ((token list))
  (getf token :type))

(defmethod contents ((token list))
  (getf token :contents))

(defmethod index ((token list))
  (getf token :index))

(defun get-single-token (str i)
  "Starting from the I-th character in STR, return the next token."
  (let ((char (elt str i))
        (len (length str)))
    (cond ((not (alphanumericp char))
           (make-token :whitespace (string char) i))
          ((consonantp char)
           (make-token :consonant (string char) i))
          (t (let* ((j (or (next-non-whitespace str (1+ i))
                           len))
                    (substr (subseq str i (min (1+ j) len))))
               (if (diphtongp substr)
                   (make-token :diphtong substr i)
                   (make-token :vowel (string char) i)))))))

(defun tokenize (str)
  "Parse STR and return a list of text tokens.
Tokens may represent a vowel. consonant or diphtong sound."
  (loop with len = (length str)
        for i fixnum = 0 then (+ i (length (contents tok)))
        while (< i len)
        for tok = (get-single-token str i)
        collect tok))

(defun replace-multiword-tokens (text)
  "Replace multiword phrases of interest in TEXT."
  ;;TODO: Cleaner, but still not the best solution, could we do better?
  (cl-ppcre:regex-replace-all "che cosa" text "che"))

(defun rebuild-string (toks)
  "Return the concatenation of the text contained in the list of TOKS."
  (reduce #'concat toks :key #'contents))

(defun get-replacement-string (str)
  "Return the appropriate replacement string for STR in Tuscan text.
This is used for operations such as translating a hard 'c' to a tuscan gorge."
  (let ((dcase-str (string-downcase str))
        (had-upcase-p (some #'upper-case-p str)))
    (let ((result (cond ((member dcase-str '("c" "q") :test #'string=) "h")
                        ((string= dcase-str "t") "th")
                        (t nil))))
      (cond ((null result) result)
            (had-upcase-p (string-upcase result))
            (t result)))))

(defun gorge-aspirate (prev-toks old-str)
  "Return the appropriate gorge aspiration for OLD-STR.
To make this decision, the list of PREV-TOKS is also scanned."
  (let ((new-str (get-replacement-string old-str)))
    (unless new-str
      (return-from gorge-aspirate old-str))
    (let ((state 1))
      (flet ((next-state ()
               (psetf prev-toks (rest prev-toks)
                      state (1+ state)))
             (skip-whitespace ()
               (setf prev-toks (rest prev-toks))))
        (loop for sym = (toktype (first prev-toks)) do
          (ecase state
            (1 (if (null prev-toks)
                   (return old-str)
                   (case sym
                     (:whitespace (skip-whitespace))
                     (:consonant (return old-str))
                     ;; (:diphtong (return old-str))
                     (otherwise (next-state)))))
            (2 (if (null prev-toks)
                   (return new-str)
                   (case sym
                     (:whitespace (return old-str))
                     ;;(:consonant (return new-str))
                     (otherwise (next-state)))))
            (3 (if (null prev-toks)
                   (return new-str)
                   (case sym
                     (:whitespace (return new-str))
                     ((:vowel :diphtong) (return new-str))
                     (otherwise (next-state)))))
            (4 (if (null prev-toks)
                   (return old-str)
                   (case sym
                     (:whitespace (skip-whitespace))
                     (:consonant (return new-str))
                     (:vowel (return new-str))
                     (otherwise (return old-str)))))))))))

(defun gorgifiable-token-p (tok next-tok)
  "Return non-NIL if TOK is gorgifiable, taking NEXT-TOK into account.
Otherwise, return NIL."
  (let ((contents (string-downcase (contents tok)))
        (next-contents (string-downcase (contents next-tok))))
    (or (and (member contents '("c" "q") :test #'string=)
             (member next-contents '("a" "o" "u") :test #'string=))
        (and (string= contents "t")
             (not (eq (toktype next-tok) :consonant))))))

(defun gorge-aspirate-tokens (toks)
  "Return a token stream with aspirated letters according to Tuscan gorge.
TOKS is destructively modified."
  (flet ((new-contents (previous-toks tok next-tok)
           (if (and next-tok (gorgifiable-token-p tok next-tok))
               (gorge-aspirate (rest previous-toks) (contents tok))
               (contents tok))))
    (loop with reversed-toks = (nreverse toks)
          for previous-toks on reversed-toks
          and tok = (first reversed-toks) then (second previous-toks)
          and next-tok = nil then tok
          collect (make-token (toktype tok)
                              (new-contents previous-toks
                                            tok
                                            next-tok)
                              (index tok))
            into result
          finally (return (nreverse result)))))

(defun gorgify-text (text)
  "Return a string containing all possible gorgifying operations on TEXT.
TEXT may contain newlines."
  (rebuild-string (gorge-aspirate-tokens (tokenize text))))

(defmethod translate-dispatch (text (language (eql :memenglish)))
  (memeify-text text))

(defmethod translate-dispatch (text (language (eql :tuscan)))
  (memeify-text (gorgify-text (replace-multiword-tokens text))))

(defun translate (text &optional lang)
  "Translate TEXT into the current language, or LANG if passed."
  (let ((prev-lang (if lang (current-language) nil)))
    (if lang
        (set-language lang)
        (setf lang *current-language*))
    (prog1
        (translate-dispatch text lang)
      (when prev-lang
        (set-language prev-lang)))))

(load-database :memenglish :tuscan)
